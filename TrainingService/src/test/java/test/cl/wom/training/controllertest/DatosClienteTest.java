package test.cl.wom.training.controllertest;

import java.net.MalformedURLException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringRunner;

import cl.wom.training.controller.DatosClienteController;

@RunWith(SpringRunner.class)
public class DatosClienteTest {
	
	@InjectMocks
	private DatosClienteController controller;
	
	@Test
	public void datosClienteTest() throws MalformedURLException {
		 Assert.assertNotNull(controller.obtenerDatosCliente("193842631"));
	}
	

}
