package test.cl.wom.training.controllertest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringRunner;

import cl.wom.training.controller.ExampleController;

@RunWith(SpringRunner.class)
public class ExampleControllerTest {

    /**
     * Clase a probar.
     */
    @InjectMocks
    private ExampleController controller;   

    @Test
    public void simpleTest() {
        Assert.assertNotNull(controller.testService(1));
    }
}
