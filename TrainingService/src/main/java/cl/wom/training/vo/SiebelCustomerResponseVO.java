package cl.wom.training.vo;

import java.util.ArrayList;
import java.util.List;

public class SiebelCustomerResponseVO {
	
	@SuppressWarnings("compatibility")
    private static final long serialVersionUID = 1L;

    public SiebelCustomerResponseVO(){
        super();
    }    
    
    private String codigoErrorSiebel = null;
    private String mensajeErrorSiebel = null;
    private String codigoErrorPgc = null;
    private String mensajeErrorPgc = null;
    private String nombre = "";
    private String apellido = "";
    private String ciudad = "";
    private String pais = "";
    private String numero = "";
    private String codigoPostal = "";
    private String estado = "";
    private String calle = "";
    private String rut = "";
    private String companyName = "";
    private String recordRowIdPgc = "";
    private String billingCustomerCodeSiebel = "";
    private String crmCustomerCodeSiebel = "";
    private String integrationIdSiebel = "";
    private List<String> listintegrationIdSiebel = new ArrayList<String>();
    private List<String> listcrmCustomerCodeSiebel = new ArrayList<String>();
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getApellido() {
        return apellido;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getPais() {
        return pais;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNumero() {
        return numero;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstado() {
        return estado;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getCalle() {
        return calle;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getRut() {
        return rut;
    }

    public void setCodigoErrorSiebel(String codigoErrorSiebel) {
        this.codigoErrorSiebel = codigoErrorSiebel;
    }

    public String getCodigoErrorSiebel() {
        return codigoErrorSiebel;
    }

    public void setMensajeErrorSiebel(String mensajeErrorSiebel) {
        this.mensajeErrorSiebel = mensajeErrorSiebel;
    }

    public String getMensajeErrorSiebel() {
        return mensajeErrorSiebel;
    }

    public void setCodigoErrorPgc(String codigoErrorPgc) {
        this.codigoErrorPgc = codigoErrorPgc;
    }

    public String getCodigoErrorPgc() {
        return codigoErrorPgc;
    }

    public void setMensajeErrorPgc(String mensajeErrorPgc) {
        this.mensajeErrorPgc = mensajeErrorPgc;
    }

    public String getMensajeErrorPgc() {
        return mensajeErrorPgc;
    }

    public void setRecordRowIdPgc(String recordRowIdPgc) {
        this.recordRowIdPgc = recordRowIdPgc;
    }

    public String getRecordRowIdPgc() {
        return recordRowIdPgc;
    }

    public void setBillingCustomerCodeSiebel(String billingCustomerCodeSiebel) {
        this.billingCustomerCodeSiebel = billingCustomerCodeSiebel;
    }

    public String getBillingCustomerCodeSiebel() {
        return billingCustomerCodeSiebel;
    }

    public void setCrmCustomerCodeSiebel(String crmCustomerCodeSiebel) {
        this.crmCustomerCodeSiebel = crmCustomerCodeSiebel;
    }

    public String getCrmCustomerCodeSiebel() {
        return crmCustomerCodeSiebel;
    }

    public void setIntegrationIdSiebel(String integrationIdSiebel) {
        this.integrationIdSiebel = integrationIdSiebel;
    }

    public String getIntegrationIdSiebel() {
        return integrationIdSiebel;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setListintegrationIdSiebel(List<String> listintegrationIdSiebel) {
        this.listintegrationIdSiebel = listintegrationIdSiebel;
    }

    public List<String> getListintegrationIdSiebel() {
        return listintegrationIdSiebel;
    }

    public void setListcrmCustomerCodeSiebel(List<String> listcrmCustomerCodeSiebel) {
        this.listcrmCustomerCodeSiebel = listcrmCustomerCodeSiebel;
    }

    public List<String> getListcrmCustomerCodeSiebel() {
        return listcrmCustomerCodeSiebel;
    }

}
