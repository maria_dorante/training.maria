package cl.wom.training.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

/**
 * Clase utilitaria para conexion a base de datos.
 *
 * @author TINet - Oscar Saavedra H.
 */
public class BaseDAO {

    /**
     * Logger.
     */
    private static Logger log = Logger.getLogger(BaseDAO.class);

    /**
     * Url.
     */
    protected String url;

    /**
     * User.
     */
    protected String user;

    /**
     * Password.
     */
    protected String password;

    /**
     * Metodo para obtener la conexion a base de datos.
     * 
     * @return connection.
     */
    public Connection getConnection() {

        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            log.error("[getConnection] Error al obtener conexion", e);
        }
        return connection;

    }

    /**
     * Metodo para cerrar una conexion.
     * 
     * @param connection connection.
     */
    public void closeConnection(Connection connection) {

        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        } catch (SQLException e) {
            log.error("[closeConnection] Error al cerrar conexion", e);
        }
    }

    /**
     * Metodo para cerrar una callable statement.
     * 
     * @param connection connection.
     */
    public void closeCallableStatement(CallableStatement cs) {

        try {
            if (cs != null && !cs.isClosed()) {
                cs.close();
            }
        } catch (SQLException e) {
            log.error("[closeCallableStatement] Error al cerrar conexion", e);
        }
    }

    /**
     * GetString.
     * 
     * @param rs rs.
     * @param key key.
     * @return string.
     */
    public String getString(ResultSet rs, String key) {

        try {
            if (rs.getObject(key) != null) {
                return String.valueOf(rs.getObject(key));
            }
        } catch (Exception e) {
            log.error("[GetString] Error al obtener el dato" + e.getMessage());
        }
        return null;
    }

    public String getUrl() {

        return url;
    }

    public void setUrl(String url) {

        this.url = url;
    }

    public String getUser() {

        return user;
    }

    public void setUser(String user) {

        this.user = user;
    }

    public String getPassword() {

        return password;
    }

    public void setPassword(String password) {

        this.password = password;
    }

}
