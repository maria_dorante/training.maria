//package cl.wom.training.dao;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//
//import org.apache.logging.log4j.Logger;
//import org.springframework.stereotype.Component;
//
//@Component
//public class ProvisionDAO extends BaseDAO{
//	
//	private static Logger logger;
//	
//	public ProvisionDAO(String user, String password, String url) {
//		super();
//		this.user = user;
//		this.password = password;
//		this.url = url;
//	}
//	
//	public String getProperty(String key) throws SQLException {
//		String property = null;
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		Connection cn = null;
//		try {
//			cn = getConnection();
//			if (cn != null) {
//				String query = "select p.value from mw_nextel_owner.property_configuration p where p.key = ? ";
//				ps = cn.prepareStatement(query);
//				ps.setString(1, key);
//				rs = ps.executeQuery();
//				if (rs.next()) {
//					property = rs.getString(1);
//					
//				}
//			}
//		} catch (Exception e) {
//			logger.error("An error has ocurried, error ="+e);
//		}finally {
//			if (cn != null && !cn.isClosed()) {
//				closeConnection(cn);
//			}
//			
//		}
//		
//		return property;
//		
//	}
//
//}
