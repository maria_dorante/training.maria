
package cl.wom.training.ws.siebel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para AssetMgmt-AssetOrderMgmt complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AssetMgmt-AssetOrderMgmt">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ContractNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Datacard" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DatacardType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Internet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RadioType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Technology" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Tethering" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlanType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TelephoneNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AssetMgmt-AssetOrderMgmt", propOrder = {
    "contractNumber",
    "datacard",
    "datacardType",
    "equipmentType",
    "internet",
    "radioType",
    "technology",
    "tethering",
    "planType",
    "telephoneNumber"
})
public class AssetMgmtAssetOrderMgmt {

    @XmlElement(name = "ContractNumber")
    protected String contractNumber;
    @XmlElement(name = "Datacard")
    protected String datacard;
    @XmlElement(name = "DatacardType")
    protected String datacardType;
    @XmlElement(name = "EquipmentType")
    protected String equipmentType;
    @XmlElement(name = "Internet")
    protected String internet;
    @XmlElement(name = "RadioType")
    protected String radioType;
    @XmlElement(name = "Technology")
    protected String technology;
    @XmlElement(name = "Tethering")
    protected String tethering;
    @XmlElement(name = "PlanType")
    protected String planType;
    @XmlElement(name = "TelephoneNumber", required = true)
    protected String telephoneNumber;

    /**
     * Obtiene el valor de la propiedad contractNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContractNumber() {
        return contractNumber;
    }

    /**
     * Define el valor de la propiedad contractNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContractNumber(String value) {
        this.contractNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad datacard.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDatacard() {
        return datacard;
    }

    /**
     * Define el valor de la propiedad datacard.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDatacard(String value) {
        this.datacard = value;
    }

    /**
     * Obtiene el valor de la propiedad datacardType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDatacardType() {
        return datacardType;
    }

    /**
     * Define el valor de la propiedad datacardType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDatacardType(String value) {
        this.datacardType = value;
    }

    /**
     * Obtiene el valor de la propiedad equipmentType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentType() {
        return equipmentType;
    }

    /**
     * Define el valor de la propiedad equipmentType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentType(String value) {
        this.equipmentType = value;
    }

    /**
     * Obtiene el valor de la propiedad internet.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInternet() {
        return internet;
    }

    /**
     * Define el valor de la propiedad internet.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInternet(String value) {
        this.internet = value;
    }

    /**
     * Obtiene el valor de la propiedad radioType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRadioType() {
        return radioType;
    }

    /**
     * Define el valor de la propiedad radioType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRadioType(String value) {
        this.radioType = value;
    }

    /**
     * Obtiene el valor de la propiedad technology.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTechnology() {
        return technology;
    }

    /**
     * Define el valor de la propiedad technology.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTechnology(String value) {
        this.technology = value;
    }

    /**
     * Obtiene el valor de la propiedad tethering.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTethering() {
        return tethering;
    }

    /**
     * Define el valor de la propiedad tethering.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTethering(String value) {
        this.tethering = value;
    }

    /**
     * Obtiene el valor de la propiedad planType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanType() {
        return planType;
    }

    /**
     * Define el valor de la propiedad planType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanType(String value) {
        this.planType = value;
    }

    /**
     * Obtiene el valor de la propiedad telephoneNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    /**
     * Define el valor de la propiedad telephoneNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelephoneNumber(String value) {
        this.telephoneNumber = value;
    }

}
