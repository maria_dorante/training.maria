
package cl.wom.training.ws.siebel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para Account complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Account">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VIP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NIIAccountExempt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="companyName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AccountType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ListOfNiiBillingAccount" type="{http://www.siebel.com/xml/NIIQueryCustomerListDataOutputIO}ListOfNiiBillingAccount" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Account", propOrder = {
    "vip",
    "niiAccountExempt",
    "companyName",
    "accountType",
    "listOfNiiBillingAccount"
})
public class Account {

    @XmlElement(name = "VIP")
    protected String vip;
    @XmlElement(name = "NIIAccountExempt")
    protected String niiAccountExempt;
    @XmlElement(required = true)
    protected String companyName;
    @XmlElement(name = "AccountType", required = true)
    protected String accountType;
    @XmlElement(name = "ListOfNiiBillingAccount")
    protected ListOfNiiBillingAccount listOfNiiBillingAccount;

    /**
     * Obtiene el valor de la propiedad vip.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVIP() {
        return vip;
    }

    /**
     * Define el valor de la propiedad vip.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVIP(String value) {
        this.vip = value;
    }

    /**
     * Obtiene el valor de la propiedad niiAccountExempt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNIIAccountExempt() {
        return niiAccountExempt;
    }

    /**
     * Define el valor de la propiedad niiAccountExempt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNIIAccountExempt(String value) {
        this.niiAccountExempt = value;
    }

    /**
     * Obtiene el valor de la propiedad companyName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Define el valor de la propiedad companyName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyName(String value) {
        this.companyName = value;
    }

    /**
     * Obtiene el valor de la propiedad accountType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountType() {
        return accountType;
    }

    /**
     * Define el valor de la propiedad accountType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountType(String value) {
        this.accountType = value;
    }

    /**
     * Obtiene el valor de la propiedad listOfNiiBillingAccount.
     * 
     * @return
     *     possible object is
     *     {@link ListOfNiiBillingAccount }
     *     
     */
    public ListOfNiiBillingAccount getListOfNiiBillingAccount() {
        return listOfNiiBillingAccount;
    }

    /**
     * Define el valor de la propiedad listOfNiiBillingAccount.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfNiiBillingAccount }
     *     
     */
    public void setListOfNiiBillingAccount(ListOfNiiBillingAccount value) {
        this.listOfNiiBillingAccount = value;
    }

}
