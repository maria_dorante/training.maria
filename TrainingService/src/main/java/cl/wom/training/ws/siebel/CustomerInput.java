
package cl.wom.training.ws.siebel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para CustomerInput complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CustomerInput">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ANI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomerID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BSCSCustCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CRMCustomerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrimaryDocumentNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerInput", namespace = "http://nii.com/EnterpriseProxyService/AccountManagement/QueryCustomerListDataWSDL/V1", propOrder = {
    "ani",
    "customerID",
    "bscsCustCode",
    "crmCustomerCode",
    "primaryDocumentNumber"
})
public class CustomerInput {

    @XmlElement(name = "ANI")
    protected String ani;
    @XmlElement(name = "CustomerID")
    protected String customerID;
    @XmlElement(name = "BSCSCustCode")
    protected String bscsCustCode;
    @XmlElement(name = "CRMCustomerCode")
    protected String crmCustomerCode;
    @XmlElement(name = "PrimaryDocumentNumber")
    protected String primaryDocumentNumber;

    /**
     * Obtiene el valor de la propiedad ani.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getANI() {
        return ani;
    }

    /**
     * Define el valor de la propiedad ani.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setANI(String value) {
        this.ani = value;
    }

    /**
     * Obtiene el valor de la propiedad customerID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerID() {
        return customerID;
    }

    /**
     * Define el valor de la propiedad customerID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerID(String value) {
        this.customerID = value;
    }

    /**
     * Obtiene el valor de la propiedad bscsCustCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBSCSCustCode() {
        return bscsCustCode;
    }

    /**
     * Define el valor de la propiedad bscsCustCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBSCSCustCode(String value) {
        this.bscsCustCode = value;
    }

    /**
     * Obtiene el valor de la propiedad crmCustomerCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCRMCustomerCode() {
        return crmCustomerCode;
    }

    /**
     * Define el valor de la propiedad crmCustomerCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCRMCustomerCode(String value) {
        this.crmCustomerCode = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryDocumentNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryDocumentNumber() {
        return primaryDocumentNumber;
    }

    /**
     * Define el valor de la propiedad primaryDocumentNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryDocumentNumber(String value) {
        this.primaryDocumentNumber = value;
    }

}
