
package cl.wom.training.ws.siebel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para Customer complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Customer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Error_spcCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Error_spcMessage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Response_spcMessage" type="{http://nii.com/EnterpriseProxyService/AccountManagement/QueryCustomerListDataWSDL/V1}Response_spcMessage"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Customer", namespace = "http://nii.com/EnterpriseProxyService/AccountManagement/QueryCustomerListDataWSDL/V1", propOrder = {
    "errorSpcCode",
    "errorSpcMessage",
    "responseSpcMessage"
})
public class Customer {

    @XmlElement(name = "Error_spcCode", required = true)
    protected String errorSpcCode;
    @XmlElement(name = "Error_spcMessage", required = true)
    protected String errorSpcMessage;
    @XmlElement(name = "Response_spcMessage", required = true)
    protected ResponseSpcMessage responseSpcMessage;

    /**
     * Obtiene el valor de la propiedad errorSpcCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorSpcCode() {
        return errorSpcCode;
    }

    /**
     * Define el valor de la propiedad errorSpcCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorSpcCode(String value) {
        this.errorSpcCode = value;
    }

    /**
     * Obtiene el valor de la propiedad errorSpcMessage.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorSpcMessage() {
        return errorSpcMessage;
    }

    /**
     * Define el valor de la propiedad errorSpcMessage.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorSpcMessage(String value) {
        this.errorSpcMessage = value;
    }

    /**
     * Obtiene el valor de la propiedad responseSpcMessage.
     * 
     * @return
     *     possible object is
     *     {@link ResponseSpcMessage }
     *     
     */
    public ResponseSpcMessage getResponseSpcMessage() {
        return responseSpcMessage;
    }

    /**
     * Define el valor de la propiedad responseSpcMessage.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseSpcMessage }
     *     
     */
    public void setResponseSpcMessage(ResponseSpcMessage value) {
        this.responseSpcMessage = value;
    }

}
