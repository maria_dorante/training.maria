
package cl.wom.training.ws.siebel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para NiiBillingAccount complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="NiiBillingAccount">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IntegrationId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NIIAccountNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NIIDocumentNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CRMCustomerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ListOfNIIBillingAccount_BusinessAddress" type="{http://www.siebel.com/xml/NIIQueryCustomerListDataOutputIO}ListOfNIIBillingAccount_BusinessAddress" minOccurs="0"/>
 *         &lt;element name="ListOfNIIBillingAccount_PrimaryContact" type="{http://www.siebel.com/xml/NIIQueryCustomerListDataOutputIO}ListOfNIIBillingAccount_PrimaryContact" minOccurs="0"/>
 *         &lt;element name="ListOfAssetMgmt-AssetOrderMgmt" type="{http://www.siebel.com/xml/NIIQueryCustomerListDataOutputIO}ListOfAssetMgmt-AssetOrderMgmt" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NiiBillingAccount", propOrder = {
    "integrationId",
    "niiAccountNumber",
    "niiDocumentNumber",
    "crmCustomerCode",
    "listOfNIIBillingAccountBusinessAddress",
    "listOfNIIBillingAccountPrimaryContact",
    "listOfAssetMgmtAssetOrderMgmt"
})
public class NiiBillingAccount {

    @XmlElement(name = "IntegrationId")
    protected String integrationId;
    @XmlElement(name = "NIIAccountNumber")
    protected String niiAccountNumber;
    @XmlElement(name = "NIIDocumentNumber")
    protected String niiDocumentNumber;
    @XmlElement(name = "CRMCustomerCode")
    protected String crmCustomerCode;
    @XmlElement(name = "ListOfNIIBillingAccount_BusinessAddress")
    protected ListOfNIIBillingAccountBusinessAddress listOfNIIBillingAccountBusinessAddress;
    @XmlElement(name = "ListOfNIIBillingAccount_PrimaryContact")
    protected ListOfNIIBillingAccountPrimaryContact listOfNIIBillingAccountPrimaryContact;
    @XmlElement(name = "ListOfAssetMgmt-AssetOrderMgmt")
    protected ListOfAssetMgmtAssetOrderMgmt listOfAssetMgmtAssetOrderMgmt;

    /**
     * Obtiene el valor de la propiedad integrationId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntegrationId() {
        return integrationId;
    }

    /**
     * Define el valor de la propiedad integrationId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntegrationId(String value) {
        this.integrationId = value;
    }

    /**
     * Obtiene el valor de la propiedad niiAccountNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNIIAccountNumber() {
        return niiAccountNumber;
    }

    /**
     * Define el valor de la propiedad niiAccountNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNIIAccountNumber(String value) {
        this.niiAccountNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad niiDocumentNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNIIDocumentNumber() {
        return niiDocumentNumber;
    }

    /**
     * Define el valor de la propiedad niiDocumentNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNIIDocumentNumber(String value) {
        this.niiDocumentNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad crmCustomerCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCRMCustomerCode() {
        return crmCustomerCode;
    }

    /**
     * Define el valor de la propiedad crmCustomerCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCRMCustomerCode(String value) {
        this.crmCustomerCode = value;
    }

    /**
     * Obtiene el valor de la propiedad listOfNIIBillingAccountBusinessAddress.
     * 
     * @return
     *     possible object is
     *     {@link ListOfNIIBillingAccountBusinessAddress }
     *     
     */
    public ListOfNIIBillingAccountBusinessAddress getListOfNIIBillingAccountBusinessAddress() {
        return listOfNIIBillingAccountBusinessAddress;
    }

    /**
     * Define el valor de la propiedad listOfNIIBillingAccountBusinessAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfNIIBillingAccountBusinessAddress }
     *     
     */
    public void setListOfNIIBillingAccountBusinessAddress(ListOfNIIBillingAccountBusinessAddress value) {
        this.listOfNIIBillingAccountBusinessAddress = value;
    }

    /**
     * Obtiene el valor de la propiedad listOfNIIBillingAccountPrimaryContact.
     * 
     * @return
     *     possible object is
     *     {@link ListOfNIIBillingAccountPrimaryContact }
     *     
     */
    public ListOfNIIBillingAccountPrimaryContact getListOfNIIBillingAccountPrimaryContact() {
        return listOfNIIBillingAccountPrimaryContact;
    }

    /**
     * Define el valor de la propiedad listOfNIIBillingAccountPrimaryContact.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfNIIBillingAccountPrimaryContact }
     *     
     */
    public void setListOfNIIBillingAccountPrimaryContact(ListOfNIIBillingAccountPrimaryContact value) {
        this.listOfNIIBillingAccountPrimaryContact = value;
    }

    /**
     * Obtiene el valor de la propiedad listOfAssetMgmtAssetOrderMgmt.
     * 
     * @return
     *     possible object is
     *     {@link ListOfAssetMgmtAssetOrderMgmt }
     *     
     */
    public ListOfAssetMgmtAssetOrderMgmt getListOfAssetMgmtAssetOrderMgmt() {
        return listOfAssetMgmtAssetOrderMgmt;
    }

    /**
     * Define el valor de la propiedad listOfAssetMgmtAssetOrderMgmt.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfAssetMgmtAssetOrderMgmt }
     *     
     */
    public void setListOfAssetMgmtAssetOrderMgmt(ListOfAssetMgmtAssetOrderMgmt value) {
        this.listOfAssetMgmtAssetOrderMgmt = value;
    }

}
