
package cl.wom.training.ws.siebel;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ListOfNIIBillingAccount_PrimaryContact complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ListOfNIIBillingAccount_PrimaryContact">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NIIBillingAccount_PrimaryContact" type="{http://www.siebel.com/xml/NIIQueryCustomerListDataOutputIO}NIIBillingAccount_PrimaryContact" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListOfNIIBillingAccount_PrimaryContact", propOrder = {
    "niiBillingAccountPrimaryContact"
})
public class ListOfNIIBillingAccountPrimaryContact {

    @XmlElement(name = "NIIBillingAccount_PrimaryContact")
    protected List<NIIBillingAccountPrimaryContact> niiBillingAccountPrimaryContact;

    /**
     * Gets the value of the niiBillingAccountPrimaryContact property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the niiBillingAccountPrimaryContact property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNIIBillingAccountPrimaryContact().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NIIBillingAccountPrimaryContact }
     * 
     * 
     */
    public List<NIIBillingAccountPrimaryContact> getNIIBillingAccountPrimaryContact() {
        if (niiBillingAccountPrimaryContact == null) {
            niiBillingAccountPrimaryContact = new ArrayList<NIIBillingAccountPrimaryContact>();
        }
        return this.niiBillingAccountPrimaryContact;
    }

}
