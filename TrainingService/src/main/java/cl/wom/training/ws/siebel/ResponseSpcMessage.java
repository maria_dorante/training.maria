
package cl.wom.training.ws.siebel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para Response_spcMessage complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Response_spcMessage">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.siebel.com/xml/NIIQueryCustomerListDataOutputIO}ListOfNiiquerycustomerlistdataoutputio"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Response_spcMessage", namespace = "http://nii.com/EnterpriseProxyService/AccountManagement/QueryCustomerListDataWSDL/V1", propOrder = {
    "listOfNiiquerycustomerlistdataoutputio"
})
public class ResponseSpcMessage {

    @XmlElement(name = "ListOfNiiquerycustomerlistdataoutputio", namespace = "http://www.siebel.com/xml/NIIQueryCustomerListDataOutputIO", required = true)
    protected ListOfNiiquerycustomerlistdataoutputio listOfNiiquerycustomerlistdataoutputio;

    /**
     * Obtiene el valor de la propiedad listOfNiiquerycustomerlistdataoutputio.
     * 
     * @return
     *     possible object is
     *     {@link ListOfNiiquerycustomerlistdataoutputio }
     *     
     */
    public ListOfNiiquerycustomerlistdataoutputio getListOfNiiquerycustomerlistdataoutputio() {
        return listOfNiiquerycustomerlistdataoutputio;
    }

    /**
     * Define el valor de la propiedad listOfNiiquerycustomerlistdataoutputio.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfNiiquerycustomerlistdataoutputio }
     *     
     */
    public void setListOfNiiquerycustomerlistdataoutputio(ListOfNiiquerycustomerlistdataoutputio value) {
        this.listOfNiiquerycustomerlistdataoutputio = value;
    }

}
