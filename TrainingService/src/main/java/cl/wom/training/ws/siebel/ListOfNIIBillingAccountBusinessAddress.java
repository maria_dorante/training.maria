
package cl.wom.training.ws.siebel;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ListOfNIIBillingAccount_BusinessAddress complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ListOfNIIBillingAccount_BusinessAddress">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NIIBillingAccount_BusinessAddress" type="{http://www.siebel.com/xml/NIIQueryCustomerListDataOutputIO}NIIBillingAccount_BusinessAddress" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListOfNIIBillingAccount_BusinessAddress", propOrder = {
    "niiBillingAccountBusinessAddress"
})
public class ListOfNIIBillingAccountBusinessAddress {

    @XmlElement(name = "NIIBillingAccount_BusinessAddress")
    protected List<NIIBillingAccountBusinessAddress> niiBillingAccountBusinessAddress;

    /**
     * Gets the value of the niiBillingAccountBusinessAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the niiBillingAccountBusinessAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNIIBillingAccountBusinessAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NIIBillingAccountBusinessAddress }
     * 
     * 
     */
    public List<NIIBillingAccountBusinessAddress> getNIIBillingAccountBusinessAddress() {
        if (niiBillingAccountBusinessAddress == null) {
            niiBillingAccountBusinessAddress = new ArrayList<NIIBillingAccountBusinessAddress>();
        }
        return this.niiBillingAccountBusinessAddress;
    }

}
