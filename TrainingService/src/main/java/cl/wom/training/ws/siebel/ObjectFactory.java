
package cl.wom.training.ws.siebel;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cl.wom.ws.siebel package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _NIICustomerDataQueryResponse_QNAME = new QName("http://nii.com/EnterpriseProxyService/AccountManagement/QueryCustomerListDataWSDL/V1", "NII_Customer_Data_QueryResponse");
    private final static QName _ListOfNiiquerycustomerlistdataoutputio_QNAME = new QName("http://www.siebel.com/xml/NIIQueryCustomerListDataOutputIO", "ListOfNiiquerycustomerlistdataoutputio");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cl.wom.ws.siebel
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ListOfNiiquerycustomerlistdataoutputio }
     * 
     */
    public ListOfNiiquerycustomerlistdataoutputio createListOfNiiquerycustomerlistdataoutputio() {
        return new ListOfNiiquerycustomerlistdataoutputio();
    }

    /**
     * Create an instance of {@link Customer }
     * 
     */
    public Customer createCustomer() {
        return new Customer();
    }

    /**
     * Create an instance of {@link ResponseSpcMessage }
     * 
     */
    public ResponseSpcMessage createResponseSpcMessage() {
        return new ResponseSpcMessage();
    }

    /**
     * Create an instance of {@link CustomerInput }
     * 
     */
    public CustomerInput createCustomerInput() {
        return new CustomerInput();
    }

    /**
     * Create an instance of {@link GetCustomerInformationResponse }
     * 
     */
    public GetCustomerInformationResponse createGetCustomerInformationResponse() {
        return new GetCustomerInformationResponse();
    }

    /**
     * Create an instance of {@link ListOfNiiquerycustomerlistdataoutputioTopElmt }
     * 
     */
    public ListOfNiiquerycustomerlistdataoutputioTopElmt createListOfNiiquerycustomerlistdataoutputioTopElmt() {
        return new ListOfNiiquerycustomerlistdataoutputioTopElmt();
    }

    /**
     * Create an instance of {@link Account }
     * 
     */
    public Account createAccount() {
        return new Account();
    }

    /**
     * Create an instance of {@link ListOfNIIBillingAccountBusinessAddress }
     * 
     */
    public ListOfNIIBillingAccountBusinessAddress createListOfNIIBillingAccountBusinessAddress() {
        return new ListOfNIIBillingAccountBusinessAddress();
    }

    /**
     * Create an instance of {@link NIIBillingAccountPrimaryContact }
     * 
     */
    public NIIBillingAccountPrimaryContact createNIIBillingAccountPrimaryContact() {
        return new NIIBillingAccountPrimaryContact();
    }

    /**
     * Create an instance of {@link NIIBillingAccountBusinessAddress }
     * 
     */
    public NIIBillingAccountBusinessAddress createNIIBillingAccountBusinessAddress() {
        return new NIIBillingAccountBusinessAddress();
    }

    /**
     * Create an instance of {@link ListOfNIIBillingAccountPrimaryContact }
     * 
     */
    public ListOfNIIBillingAccountPrimaryContact createListOfNIIBillingAccountPrimaryContact() {
        return new ListOfNIIBillingAccountPrimaryContact();
    }

    /**
     * Create an instance of {@link ListOfNiiBillingAccount }
     * 
     */
    public ListOfNiiBillingAccount createListOfNiiBillingAccount() {
        return new ListOfNiiBillingAccount();
    }

    /**
     * Create an instance of {@link NiiBillingAccount }
     * 
     */
    public NiiBillingAccount createNiiBillingAccount() {
        return new NiiBillingAccount();
    }

    /**
     * Create an instance of {@link ListOfAssetMgmtAssetOrderMgmt }
     * 
     */
    public ListOfAssetMgmtAssetOrderMgmt createListOfAssetMgmtAssetOrderMgmt() {
        return new ListOfAssetMgmtAssetOrderMgmt();
    }

    /**
     * Create an instance of {@link AssetMgmtAssetOrderMgmt }
     * 
     */
    public AssetMgmtAssetOrderMgmt createAssetMgmtAssetOrderMgmt() {
        return new AssetMgmtAssetOrderMgmt();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListOfNiiquerycustomerlistdataoutputio }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://nii.com/EnterpriseProxyService/AccountManagement/QueryCustomerListDataWSDL/V1", name = "NII_Customer_Data_QueryResponse")
    public JAXBElement<ListOfNiiquerycustomerlistdataoutputio> createNIICustomerDataQueryResponse(ListOfNiiquerycustomerlistdataoutputio value) {
        return new JAXBElement<ListOfNiiquerycustomerlistdataoutputio>(_NIICustomerDataQueryResponse_QNAME, ListOfNiiquerycustomerlistdataoutputio.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListOfNiiquerycustomerlistdataoutputio }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.siebel.com/xml/NIIQueryCustomerListDataOutputIO", name = "ListOfNiiquerycustomerlistdataoutputio")
    public JAXBElement<ListOfNiiquerycustomerlistdataoutputio> createListOfNiiquerycustomerlistdataoutputio(ListOfNiiquerycustomerlistdataoutputio value) {
        return new JAXBElement<ListOfNiiquerycustomerlistdataoutputio>(_ListOfNiiquerycustomerlistdataoutputio_QNAME, ListOfNiiquerycustomerlistdataoutputio.class, null, value);
    }

}
