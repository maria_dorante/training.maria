
package cl.wom.training.ws.siebel;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ListOfAssetMgmt-AssetOrderMgmt complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ListOfAssetMgmt-AssetOrderMgmt">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AssetMgmt-AssetOrderMgmt" type="{http://www.siebel.com/xml/NIIQueryCustomerListDataOutputIO}AssetMgmt-AssetOrderMgmt" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListOfAssetMgmt-AssetOrderMgmt", propOrder = {
    "assetMgmtAssetOrderMgmt"
})
public class ListOfAssetMgmtAssetOrderMgmt {

    @XmlElement(name = "AssetMgmt-AssetOrderMgmt")
    protected List<AssetMgmtAssetOrderMgmt> assetMgmtAssetOrderMgmt;

    /**
     * Gets the value of the assetMgmtAssetOrderMgmt property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the assetMgmtAssetOrderMgmt property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssetMgmtAssetOrderMgmt().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AssetMgmtAssetOrderMgmt }
     * 
     * 
     */
    public List<AssetMgmtAssetOrderMgmt> getAssetMgmtAssetOrderMgmt() {
        if (assetMgmtAssetOrderMgmt == null) {
            assetMgmtAssetOrderMgmt = new ArrayList<AssetMgmtAssetOrderMgmt>();
        }
        return this.assetMgmtAssetOrderMgmt;
    }

}
