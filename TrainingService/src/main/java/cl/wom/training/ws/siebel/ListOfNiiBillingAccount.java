
package cl.wom.training.ws.siebel;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ListOfNiiBillingAccount complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ListOfNiiBillingAccount">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NiiBillingAccount" type="{http://www.siebel.com/xml/NIIQueryCustomerListDataOutputIO}NiiBillingAccount" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListOfNiiBillingAccount", propOrder = {
    "niiBillingAccount"
})
public class ListOfNiiBillingAccount {

    @XmlElement(name = "NiiBillingAccount")
    protected List<NiiBillingAccount> niiBillingAccount;

    /**
     * Gets the value of the niiBillingAccount property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the niiBillingAccount property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNiiBillingAccount().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NiiBillingAccount }
     * 
     * 
     */
    public List<NiiBillingAccount> getNiiBillingAccount() {
        if (niiBillingAccount == null) {
            niiBillingAccount = new ArrayList<NiiBillingAccount>();
        }
        return this.niiBillingAccount;
    }

}
