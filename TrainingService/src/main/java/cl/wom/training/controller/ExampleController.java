package cl.wom.training.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.wom.training.to.ResponseTO;

@RestController
@RequestMapping("/trainingService/rest/")
@PropertySource("file:${APP_ENV}")
public class ExampleController {

	private static Logger log = Logger.getLogger(ExampleController.class);

	@Value("${prop.alguna.property}")
	private String someProperty;

	@Value("${prop.test.maria}")
	private String propertyMaria;


	@GetMapping(value = "/testService/{solicitudId}")
	public ResponseTO testService(@PathVariable long solicitudId) {
		log.info("[testService][" + solicitudId + "] Inicio.");
		ResponseTO response = new ResponseTO();
		response.setMensaje(someProperty);
		log.info("[testService][" + solicitudId + "] Fin.");
		return response;
	}

	@GetMapping(value = "/testServiceMaria/{solicitudId}")
	public ResponseTO testServiceMaria(@PathVariable long solicitudId) {
		ResponseTO response = new ResponseTO();
		response.setMensaje(propertyMaria);
		return response;
	}

}
