package cl.wom.training.controller;

import java.net.MalformedURLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.wom.training.entity.ClienteEntity;
import cl.wom.training.helper.SiebelHelper;
import cl.wom.training.repository.ClienteRepository;
import cl.wom.training.to.ResponseTO;
import cl.wom.training.vo.SiebelCustomerResponseVO;

@RestController
@RequestMapping("/trainingService/rest/")
@PropertySource("file:${APP_ENV}")
public class DatosClienteController {

	@Autowired
	private ClienteRepository repository;

	@GetMapping(value = "/obtenerDatosCliente/{rut}")
	public ResponseTO obtenerDatosCliente(@PathVariable String rut) throws MalformedURLException {
		ResponseTO responseTO = new ResponseTO();
		SiebelHelper siebelHelper = new SiebelHelper();
		SiebelCustomerResponseVO responseVO = siebelHelper.datosCliente(rut);
		if (responseVO != null ) {
			
			if (responseVO.getCodigoErrorSiebel() != null && responseVO.getCodigoErrorSiebel().contentEquals("ERROR_GENERAL_000")) {
				responseTO.setMensaje("No se encontró información asociado al rut("+rut+"), por favor verifique.");
				responseTO.setStatus("failure");
				return responseTO;
			}
			
			responseTO.setMensaje("Nombre cliente: " + responseVO.getNombre() + responseVO.getApellido()
					+ ", Direccion: " + responseVO.getCalle());
			responseTO.setStatus("OK");

			ClienteEntity clienteEntity = new ClienteEntity();
			clienteEntity.setNombre(responseVO.getNombre() + responseVO.getApellido());
			clienteEntity.setDireccion(responseVO.getCalle());
			repository.save(clienteEntity);
			
		}else {
			responseTO.setMensaje("Ocurrió un error consultando el servicio, el response es null.");
			responseTO.setStatus("Failure");
			
		}

		return responseTO;
	}

}
