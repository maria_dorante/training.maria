package cl.wom.training.entity;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class ClienteEntity {
	
	private String nombre;
	private String rut;
	private String direccion;
	
	@Id
	private ObjectId id;
	
	public ClienteEntity() {
		
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}
	
}
