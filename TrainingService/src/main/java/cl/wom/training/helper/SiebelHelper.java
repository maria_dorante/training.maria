package cl.wom.training.helper;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.log4j.Logger;

import cl.wom.training.vo.SiebelCustomerResponseVO;
import cl.wom.training.ws.siebel.Customer;
import cl.wom.training.ws.siebel.ListOfNiiquerycustomerlistdataoutputio;
import cl.wom.training.ws.siebel.NIIIVRWS;
import cl.wom.training.ws.siebel.QueryCustomerPort;
import cl.wom.training.ws.siebel.ResponseSpcMessage;

public class SiebelHelper {

	private static Logger logger = Logger.getLogger(SiebelHelper.class);
	private static String URL = "http://10.120.241.20:8011/QueryCustomerListDataPrj/ProxyServices/QueryCustomerListDataPS?wsdl";

	public SiebelCustomerResponseVO datosCliente(String rutCliente) throws MalformedURLException {
		SiebelCustomerResponseVO responseVO = new SiebelCustomerResponseVO();
		try {

			if (rutCliente == null) {
				responseVO.setCodigoErrorSiebel("ERROR_RUT_IS_NULL_001");
				responseVO.setMensajeErrorSiebel("Parámetros de entrada incorrectos, Rut es null.");
				return responseVO;
			}

			URL endpoint = new URL(URL);

			cl.wom.training.ws.siebel.CustomerInput input = new cl.wom.training.ws.siebel.CustomerInput();
			rutCliente = rutCliente.replace(".", "").replace("-", "");
			input.setPrimaryDocumentNumber(rutCliente);

			NIIIVRWS servicio = new NIIIVRWS(endpoint);

			QueryCustomerPort puerto = servicio.getQueryCustomerPort();

			Customer customer = puerto.getCustomerInformation(input);

			if (null == customer) {
				logger.error("-->datosCliente: response is null");
				responseVO.setCodigoErrorSiebel("ERROR_RESPONSE_NULL_001");
				responseVO.setMensajeErrorSiebel("Error el response es null");
				return responseVO;

			} else {
				ResponseSpcMessage spcMessage = customer.getResponseSpcMessage();
				if (spcMessage != null) {
					ListOfNiiquerycustomerlistdataoutputio data = spcMessage
							.getListOfNiiquerycustomerlistdataoutputio();
					String nombre = data.getAccount().get(0).getCompanyName();
					String direccion = data.getAccount().get(0).getListOfNiiBillingAccount().getNiiBillingAccount()
							.get(0).getListOfNIIBillingAccountBusinessAddress().getNIIBillingAccountBusinessAddress()
							.get(0).getStreetAddress();

					responseVO.setRut(rutCliente);
					responseVO.setNombre(nombre);
					responseVO.setCalle(direccion);

				}

			}

		} catch (Exception e) {
			responseVO.setCodigoErrorSiebel("ERROR_GENERAL_000");
			responseVO.setMensajeErrorSiebel("Ocurrió un error al realizar la consulta al servicio.");
			logger.error("Error en la consulta al servicio de Siebel : " + e.getMessage());
		}

		return responseVO;
	}

}
