//package cl.wom.training;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.PropertySource;
//import org.springframework.stereotype.Component;
//
//@Component
//@PropertySource("file:${APP_ENV}")
//public class Propiedades{
//	
//	@Value("${prop.bd.user}")
//    private String user;
//	
//	@Value("${prop.bd.password}")
//    private String password;
//	
//	@Value("${prop.bd.urlConnection}")
//    private String url;
//	
//	public Propiedades(String user, String password, String url) {
//		this.user = user;
//		this.password = password;
//		this.url = url;
//	}
//	
//	public String getUser() {
//		return user;
//	}
//
//	public void setUser(String user) {
//		this.user = user;
//	}
//
//	public String getPassword() {
//		return password;
//	}
//
//	public void setPassword(String password) {
//		this.password = password;
//	}
//
//	public String getUrlConnection() {
//		return url;
//	}
//
//	public void setUrlConnection(String url) {
//		this.url = url;
//	}
//
//}
