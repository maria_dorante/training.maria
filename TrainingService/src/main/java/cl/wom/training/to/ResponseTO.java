package cl.wom.training.to;

import java.io.Serializable;

public class ResponseTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String status;

    private String mensaje;

    public String getStatus() {

        return status;
    }

    public void setStatus(String status) {

        this.status = status;
    }

    public String getMensaje() {

        return mensaje;
    }

    public void setMensaje(String mensaje) {

        this.mensaje = mensaje;
    }
   
}
