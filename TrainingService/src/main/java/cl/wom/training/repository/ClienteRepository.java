package cl.wom.training.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import cl.wom.training.entity.ClienteEntity;

public interface ClienteRepository extends MongoRepository<ClienteEntity, String> {
	

}
